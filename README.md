# bantam-effects

## Purpose

The `bantam-effects` module implements a set of CSS entry animations that are applied to elements as they enter the view window. Each element that should receive an effect will have the `bt-effect` and effect type classes set.

## Installation

The CSS can be downloaded [here](files/1.0/bantam-effects-1.0.min.css) and the Javascript [here](files/1.0/bantam-effects-1.0.min.js). These files can be linked directly into your app (they require [BantamJS](https://canderegg.gitlab.io/bantam-js/) as a prerequisite). Alternatively, `bantam-effects` is a module available through the [Bantam Coop](https://canderegg.gitlab.io/bantam-coop/) module manager.

To install the module through Bantam Coop, use the following command:

```bash
$ python coop.py add bantam-effects +1.0
```

## Usage

There are seven animation types that can be applied to elements with your page: `effect-left` slides the element from left to right, `effect-right` slides the element from right to left, `effect-top` slides the element from top to bottom, `effect-bottom` slides the element from bottom to top, `effect-grow` grows the element to size, `effect-shrink` shrinks the element to size, and `effect-fade` fades the element in.

The `effect-fade` effect can be combined with others, so the classes `bt-effect effect-left effect-fade` will fade in an element as it slides into place from the left. Other effects do not combine with each other.

Each effect will be applied to the element as soon as the element enters the view screen in the vertical axis.

A code example with each effect type might be written as follows:

```html
<img src="test.png" alt="Effect: Slide from left" class="bt-effect effect-left">

<img src="test.png" alt="Effect: Slide from right" class="bt-effect effect-right">

<img src="test.png" alt="Effect: Slide from top" class="bt-effect effect-top">

<img src="test.png" alt="Effect: Slide from bottom" class="bt-effect effect-bottom">

<img src="test.png" alt="Effect: Grow to fit" class="bt-effect effect-grow">

<img src="test.png" alt="Effect: Shrink to fit" class="bt-effect effect-shrink">

<img src="test.png" alt="Effect: Fade in" class="bt-effect effect-fade">
```

## License

This module is distributed under the [MIT License](LICENSE).
