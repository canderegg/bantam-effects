(function() {
  $bt.effects = {};

  $bt.effects.classes = ['effect-left', 'effect-right', 'effect-top', 
    'effect-bottom', 'effect-grow', 'effect-shrink', 'effect-fade'];

  $bt.effects.apply = function(elem) {
    for (let i=0; i<$bt.effects.classes.length; i++) {
      elem.erase($bt.effects.classes[i]);
    }
  };

  $bt.effects.queue = $bt.get('.bt-effect');

  $bt.effects.checkAll = function() {
    for (let i=0; i<$bt.effects.queue.length; i++) {
      let elem = $bt.effects.queue[i];
      let bounds = elem.getBoundingClientRect();
      if (bounds.bottom >= 0 
          && bounds.top <= (window.innerHeight || document.documentElement.clientHeight)) {
        $bt.effects.apply(elem);
      }
    }
  };

  window.onscroll = $bt.effects.checkAll;
  $bt.effects.checkAll();

})();
